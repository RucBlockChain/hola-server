#include <iostream>
#include "wfrest/HttpServer.h"
#include "wfrest/json.hpp"
#include <iostream>
#include <string>
#include <chrono>
#include <string_view>

#include "proxy.h"
#include "rsa.h"

using namespace wfrest;
using Json = nlohmann::json;


//计时器
class Timer {
private:
    std::string title;
    std::chrono::high_resolution_clock::time_point m_start, m_stop;
public:
    Timer(const std::string &title) : title(title) {
        m_start = std::chrono::high_resolution_clock::now();
    }

    ~Timer() {
        stop();
    }

    void stop() {
        m_stop = std::chrono::high_resolution_clock::now();
        std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(m_stop - m_start);
        std::cout << title << " " << (ms.count()) * 0.001 << "s\n";
    }
};


int main() {
    {
        double balance[5] = {1000.0, 2.0, 3.4, 7.0, 50.0};
        double *balanc = balance;

        double arr[2][4] = {{1, 2, 3, 4},
                            {5, 6, 7, 8}};


        double *px[5];
        px[0] = new double(12);
        double cc = 123;
        px[1] = &cc;
        // 原始明文

        int (*p)[7];
        int array[3][7] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21};
        p = array;
    }

    std::string src_text = "test begin\n this is an rsa test example!!! this is an rsa test example!!! this is an rsa test example!!! this is an rsa test example!!! this is an rsa test example!!! this is an rsa test example!!! this is an rsa test example!!! this is an rsa test example!!! this is an rsa test example!!! this is an rsa test example!!! this is an rsa test example!!! this is an rsa test example!!! this is an rsa test example!!! \ntest end";
    //src_text = "rsa test";



    std::string encrypt_text;
    std::string decrypt_text;

    // test for rsa only
    {
        // 生成密钥对
        std::string pub_key;
        std::string pri_key;
        GenerateRSAKey(pub_key, pri_key);
        printf("public key:\n");
        printf("%s\n", pub_key.c_str());
        printf("private key:\n");
        printf("%s\n", pri_key.c_str());

        // 私钥加密-公钥解密
        encrypt_text = RsaPriEncrypt(src_text, pri_key);
        printf("encrypt: len=%d\n", encrypt_text.length());
        decrypt_text = RsaPubDecrypt(encrypt_text, pub_key);
        printf("decrypt: len=%d\n", decrypt_text.length());
        printf("decrypt: %s\n", decrypt_text.c_str());

        // 公钥加密-私钥解密
        encrypt_text = RsaPubEncrypt(src_text, pub_key);
        printf("encrypt: len=%d\n", encrypt_text.length());
        decrypt_text = RsaPriDecrypt(encrypt_text, pri_key);
        printf("decrypt: len=%d\n", decrypt_text.length());
        printf("decrypt: %s\n", decrypt_text.c_str());
    }


    proxy.makeRouter(); // make a router
    proxy.start(); // start the server

    return 0;
}
