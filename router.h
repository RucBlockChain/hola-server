//
// Created by chenpeigen on 1/16/23.
//

#ifndef HOLA_SERVER_ROUTER_H
#define HOLA_SERVER_ROUTER_H

#include <wfrest/HttpServer.h>

void router(wfrest::HttpServer &svr);

#endif //HOLA_SERVER_ROUTER_H
