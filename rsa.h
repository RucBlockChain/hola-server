//
// Created by chenpeigen on 1/16/23.
//

#ifndef HOLA_SERVER_RSA_H
#define HOLA_SERVER_RSA_H


#include <string>

void GenerateRSAKey(std::string &out_pub_key, std::string &out_pri_key);

std::string RsaPriEncrypt(const std::string &clear_text, std::string &pri_key);

std::string RsaPubDecrypt(const std::string &cipher_text, const std::string &pub_key);

std::string RsaPubEncrypt(const std::string &clear_text, const std::string &pub_key);

std::string RsaPriDecrypt(const std::string &cipher_text, const std::string &pri_key);

#endif //HOLA_SERVER_RSA_H
