cmake_minimum_required(VERSION 3.24)
project(hola_server)

set(CMAKE_CXX_STANDARD 11)

add_executable(hola_server main.cpp proxy.cpp proxy.h router.h router.cpp rsa.cpp rsa.h curl.h curl.cpp)
target_link_libraries(hola_server wfrest) #关键，如果不加会报错，编译的时候找不到函数
target_link_libraries(hola_server curl) #关键，如果不加会报错，编译的时候找不到函数
target_link_libraries(hola_server crypto) #关键，如果不加会报错，编译的时候找不到函数